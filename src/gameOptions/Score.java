package gameOptions;

/**
 * Created by anton on 20.09.2017.
 */
public class Score {
   private String playerOneName;
    private String playerTwoName;
    private int playerOneScore;
    private int playerTwoScore;
    private String playScore;
   private String playerName;


    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public String getPlayScore() {
        return playScore;
    }

    public void setPlayScore(String playScore) {
        this.playScore = playScore;
    }

    public Score(int playerOneScore, int playerTwoScore,String playerOneName,String playerTwoName) {
        this.playerOneScore = playerOneScore;
        this.playerTwoScore = playerTwoScore;
        this.playerOneName = playerOneName;
        this.playerTwoName = playerTwoName;
        this.playScore= playerOneScore+" : "+playerTwoScore;
        this.playerName = playerOneName+" / "+playerTwoName;

    }

    public int getPlayerOneScore() {
        return playerOneScore;
    }

    public void setPlayerOneScore(int playerOneScore) {
        this.playerOneScore = playerOneScore;
    }

    public int getPlayerTwoScore() {
        return playerTwoScore;
    }

    public void setPlayerTwoScore(int playerTwoScore) {
        this.playerTwoScore = playerTwoScore;
    }
}
