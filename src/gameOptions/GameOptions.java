package gameOptions;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anton on 20.09.2017.
 */
public class GameOptions {
    private String firstPlayerName;
    private String secondPlayerName;
    private boolean soundOff;
    private Score score;
    private List<Score> scores;
    private boolean currentGamePause;
    private static volatile GameOptions instanse;

    private GameOptions() {
        firstPlayerName = "";
        secondPlayerName = "";
        score = new Score(0,0,"","");
        scores =new ArrayList<>();
    }

    public static  GameOptions getInstanse(){
        if (instanse==null){
            synchronized (GameOptions.class){
                if (instanse==null){
                    instanse=new GameOptions();
                }
            }
        }
        return instanse;
    }

    public String getFirstPlayerName() {
        return firstPlayerName;
    }

    public void setFirstPlayerName(String firstPlayerName) {
        this.firstPlayerName = firstPlayerName;
    }

    public String getSecondPlayerName() {
        return secondPlayerName;
    }

    public void setSecondPlayerName(String secondPlayerName) {
        this.secondPlayerName = secondPlayerName;
    }

    public boolean isSoundOff() {
        return soundOff;
    }

    public void setSoundOff(boolean soundOff) {
        this.soundOff = soundOff;
    }

    public Score getScore() {
        return score;
    }

    public void setScore(Score score) {
        this.score = score;
    }

    public List<Score> getScores() {
        return scores;
    }

    public void setScores(List<Score> scores) {
        this.scores = scores;
    }

    public boolean isCurrentGamePause() {
        return currentGamePause;
    }

    public void setCurrentGamePause(boolean currentGamePause) {
        this.currentGamePause = currentGamePause;
    }

    public static void setInstanse(GameOptions instanse) {
        GameOptions.instanse = instanse;
    }
}