import com.screens.ScreenManager;
import com.screens.Screens;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 * Created by anton on 17.09.2017.
 */
public class Main extends Application{
    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        ScreenManager.getInstance().setWindow(primaryStage);
        ScreenManager.getInstance().switchScreen(Screens.MAIN_SCREEN);
    }
}
