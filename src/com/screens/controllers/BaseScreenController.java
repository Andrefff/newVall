package com.screens.controllers;

import com.screens.ScreenManager;
import com.screens.Screens;
import javafx.application.Platform;
import javafx.fxml.Initializable;

import java.io.IOException;

/**
 * Created by anton on 20.09.2017.
 */
public abstract class BaseScreenController implements Initializable {
    protected void navigationButtonPressed(Screens screen){
        if(screen==null){
            Platform.exit();
            return;
        }
        try {
            ScreenManager.getInstance().switchScreen(screen);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
