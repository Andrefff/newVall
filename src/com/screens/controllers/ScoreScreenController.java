package com.screens.controllers;

import com.screens.Screens;
import gameOptions.GameOptions;
import gameOptions.Score;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by anton on 17.09.2017.
 */
public class ScoreScreenController extends BaseScreenController {
    public Button backScoreByt;
    public Button resetScoreByt;
    public TableView scoreTable;
    public TableColumn<Score,String> nameColump;
    public TableColumn<Score,String> scoreColump;
    private ObservableList<Score> scores;

    public void backScoreBytClicked(MouseEvent mouseEvent) {
        navigationButtonPressed(Screens.MAIN_SCREEN);

    }

    public void resetScoreByt(MouseEvent mouseEvent) {
        scores.clear();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        scores = FXCollections.observableArrayList(GameOptions.getInstanse().getScores());
         nameColump.setCellValueFactory(new PropertyValueFactory<Score,String>("playerName"));
         scoreColump.setCellValueFactory(new PropertyValueFactory<Score,String>("playScore"));

         scoreTable.setItems(scores);


    }
}
