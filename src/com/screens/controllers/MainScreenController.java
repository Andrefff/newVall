package com.screens.controllers;

import com.screens.ScreenManager;
import com.screens.Screens;
import gameOptions.GameOptions;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.media.MediaPlayer;

import javax.print.attribute.standard.Media;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by anton on 17.09.2017.
 */
public class MainScreenController extends BaseScreenController{
    public Button scoreMainBtn;
    public Button exitMainBtn;
    public TextField gameOneMainTF;
    public TextField gameTwoMainTF;
    public Button startMainBtn;
    public Button soundMainBtn;

    public void scoreBtnClicked(MouseEvent mouseEvent) {
        navigationButtonPressed(Screens.SCORE_SCREEN);

    }

    public void exitBtnClicked(MouseEvent mouseEvent) {
        navigationButtonPressed(null);

    }

    public void playBtnClicked(MouseEvent mouseEvent) throws IOException {
        GameOptions.getInstanse().setFirstPlayerName(gameOneMainTF.getText());
        GameOptions.getInstanse().setSecondPlayerName(gameTwoMainTF.getText());

        navigationButtonPressed(Screens.PLAY_SCREEN);


    }

    public void soundBtnClicked(MouseEvent mouseEvent) {
        if(!GameOptions.getInstanse().isSoundOff()){

            soundMainBtn.setStyle(" -fx-background-image: url('images/mainScreenVeiws/music.png')");
            GameOptions.getInstanse().setSoundOff(true);
            String file = "musicfile.mp3";
            Media sound = new Media(new File(file).toURI().toString());
            MediaPlayer mediaPlayer = new MediaPlayer(sound);
            mediaPlayer.play();
        }else {soundMainBtn.setStyle(" -fx-background-image: url('images/mainScreenVeiws/m.png')");
            GameOptions.getInstanse().setSoundOff(false);}
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
